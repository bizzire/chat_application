import { ChatEngine } from "react-chat-engine";

import ChatFeed from './components/ChatFeed';

import "./App.css";

const App = () => {
  return (
    <ChatEngine
      height="100vh"
      projectID="8098b2bf-9336-4e76-9272-c86a05c4f272"
      userName="HotdogLover246"
      userSecret="123456"
      renderChatFeed={(chatAppProps) => <ChatFeed {... chatAppProps } />}
    />
  );
};

export default App;
